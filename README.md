<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/chris.weaver87/repeater-website">
    <img src="repeater_website_logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Repeater Website</h3>

  <p align="center">
    A project to help Amateur Radio repeater owners have their own interactive website 
    with QSO map, connection instructions, and more. 
    <br />
    <br />
    <a href="http://gb3or.com">View Demo</a>
    ·
    <a href="https://gitlab.com/chris.weaver87/repeater-website/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/chris.weaver87/repeater-website/issues">Request Feature</a>
  </p>
</div>

### Technologies Used

* Python
* Django
* Docker
* What3Words
* Mapbox
* Bootstrap

### Demo
We have a running instance at [http://gb3or.com](http://gb3or.com) for the North Wales GB3OR repeater on top of the Great Orme.

## Installing and running

### Requirements

* [Mapbox access token](https://docs.mapbox.com/help/getting-started/access-tokens/)
* [What3Words token ](https://developer.what3words.com/public-api)
* Server with [Docker](https://docs.docker.com/get-docker/), [docker-compose](https://docs.docker.com/compose/install/), and [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) installed

### Running

1. Clone the repository
1. Copy the environment file template to the actual file `cp .env.template .env`
1. Fill in the missing environment variables in `.env`
1. Start the gunicorn server with `docker-compose up`
1. Visit [localhost:8000/admin](http://localhost:8000/admin) using the ADMIN_USERNAME and ADMIN_PASSWORD to login
1. Fill in the front_page_intro field in the SiteContent model

<!-- LICENSE -->
## License

Distributed under the GNU GPLv3 License. See `LICENSE` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>


## Contact

Chris Weaver - chris0030@pm.me

Project Link: [https://gitlab.com/chris.weaver87/repeater-website](https://gitlab.com/chris.weaver87/repeater-website)

<p align="right">(<a href="#top">back to top</a>)</p>