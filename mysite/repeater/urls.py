from django.urls import path

from . import views

app_name = 'repeater'
urlpatterns = [
        path('', views.index, name='index'),
	path('map/', views.map, name='map'),
        path('qso/', views.qso, name='qso')
]
