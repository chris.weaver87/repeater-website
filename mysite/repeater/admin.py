from django.contrib import admin

from .models import Qso, SiteContent

admin.site.register(Qso)
admin.site.register(SiteContent)
