from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.conf import settings
from django.contrib import messages

from .models import Qso, SiteContent
from django.utils import timezone

import logging
import what3words

def index(request):
    site_content = SiteContent.objects.first()
    context = {
        'front_page_intro': site_content.front_page_intro,
    }
    return render(request, 'repeater/index.html', context)

def map(request):
    qso_list = Qso.objects.all().filter(verified=True)
    context = {'qso_list': qso_list}
    return render(request, 'repeater/map.html', context)

def qso(request):
    if request.method == 'POST':
        logging.info(f"QSO being sent: {request.POST}")
        callsign =  request.POST['callsign'].upper()
        if len(callsign) > 8:
             messages.error(request, "Callsign can only be 8 characters long. For mobile/portable just add /m or /p")
             return render(request, 'repeater/qso.html')
        signal_strength = request.POST['signal_strength']
        try:
            int(signal_strength)
        except ValueError:
            messages.error(request, "Your signal strenth should be a number between 1 and 9")
            return render(request, 'repeater/qso.html')
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
        if longitude and latitude:
            try:
                float(longitude)
                float(latitude)
            except:
                messages.error(request, "Your longitude and latitude should be in decimal format")
                return render(request, 'repeater/qso.html')
        if 'what3words' in request.POST and request.POST['what3words']:
            if settings.WHAT3WORDS_KEY:
                geocoder = what3words.Geocoder(settings.WHAT3WORDS_KEY)
                coords = geocoder.convert_to_coordinates(request.POST['what3words'])
                print(coords)
                if 'square' in coords:
                    longitude = coords['square']['southwest']['lng']
                    latitude = coords['square']['southwest']['lat']
                else:
                    messages.error(request, "Your What3Words reference was incorrect")
                    return render(request, 'repeater/qso.html')
            else:
                messages.error(request, "What3Words is not set up on this server, please enter latitude and longitude")
                return render(request, 'repeater/qso.html')
        qso = Qso(
            callsign=callsign,
            signal_strength=signal_strength,
            verified=False, 
            longitude=longitude,
            latitude=latitude,
            created_date=timezone.now()
        )
        try:
            qso.save()
        except Exception as e:
            logging.error(f"QSO unable to be saved - callsign: {callsign} signal: {signal_strength} long: {longitude} lat: {latitude}")
            messages.error(request, f"Something went wrong, please email {settings.ADMIN_EMAIL}")
            return render(request, 'repeater/qso.html')
        logging.info(f"QSO added to database {qso}")
        messages.success(request, "Your QSO has been added! We'll verify it and then add it to the map")
        return HttpResponseRedirect(reverse('repeater:index'))
    return render(request, 'repeater/qso.html')
