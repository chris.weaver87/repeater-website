from django.conf import settings

def repeater_processor(request):
    repeater_settings = {
        'repeater_name': settings.RPTR_NAME,
        'repeater_tx_freq': settings.RPTR_TX_FREQ,
        'repeater_rx_freq': settings.RPTR_RX_FREQ,
        'repeater_shift': settings.RPTR_SHIFT,
        'repeater_tone': settings.RPTR_TONE,
        'repeater_lat': settings.RPTR_LAT,
        'repeater_long': settings.RPTR_LONG,
        'mapbox_token': settings.MAPBOX_TOKEN,
    }
    return repeater_settings