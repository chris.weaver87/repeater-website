from django.db import models
import arrow
from geopy import distance
import re

# Create your models here.
class Qso(models.Model):
    callsign = models.CharField(max_length=8)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    latitude = models.DecimalField(max_digits=8, decimal_places=6)
    signal_strength = models.IntegerField(null=True)
    created_date = models.DateTimeField('date created')
    verified = models.BooleanField(default=False)

    def __str__(self):
        string = f'{self.callsign}'
        if not self.verified:
            string += " UNVERIFIED"
        return string

    def escaped_callsign(self):
        return re.sub('[^A-Za-z0-9 ]+', '', self.callsign)

    def return_color(self):
        if not self.signal_strength:
            return 'blue'
        if self.signal_strength >= 8:
            return 'green'
        elif self.signal_strength < 8 and self.signal_strength >= 5:
            return 'orange'
        else:
            return 'red'

    def return_human_date(self):
        friendly_date = arrow.get(self.created_date)
        return friendly_date.humanize()

    def return_distance_to_repeater(self):
        repeater_coords = (53.332787, -3.853334)
        return int(distance.distance(repeater_coords, (self.latitude, self.longitude)).miles)

class SiteContent(models.Model):
    front_page_intro = models.TextField()
    